# Gulp-Webapp #
A gulp webapp scaffold from the yeoman `generator-gulp-webapp` with my own flavor of dependencies to get things started quickly.

## How to use. ##
After cloning, run the following commands from the project folder to ensure all dependencies are met and the bower components are included in the jade templates.

```
npm install
bower update
gulp wiredep
```

Once you have completed each of these, you can build or start a livereload server to begin working on the project.

```
gulp build

gulp watch
```


